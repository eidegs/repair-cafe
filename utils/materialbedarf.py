#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create a csv list of materials by iterating over the .md files

import os
import sys
import re

# create a list of .md-files in the ../docs directory recursively
md_files = []
for root, dirs, files in os.walk("../docs"):
    for file in files:
        if file.endswith(".md") and file != "_templ.md":
            md_files.append(os.path.join(root, file))

# iterate over md_files, collect Material items
material = []
in_mat_section = False
for file in md_files:
    for line in open(file):
        if "## Material" in line:
            in_mat_section = True
            continue
        if line.startswith("- s. [["):
            continue
        if in_mat_section:
            if line == "\n":
                in_mat_section = False
                break
            material.append(line.strip())

# remove "- " at beginning of each line of material
material = [re.sub(r'^-\s', '', line, count = 1) for line in material]

# remove duplicates in material and sort alphabetically
material = sorted(list(set(material)))

# write materialbedarf.csv to ../build
with open("../build/materialbedarf.csv", "w") as f:
    f.write('"Material","Status"\n')
    for line in material:
        f.write(f'"{line}","Unbekannt"\n')

print(f"Found {len(material)} items! All done!")
