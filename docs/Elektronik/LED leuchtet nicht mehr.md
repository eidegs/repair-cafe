#elektronik #led #lampe

Die LED an einem Gerät ist mutmaßlich defekt.

## Maßnahmen
- Prüfen der LED mit einem Digitalen Multimeter (DMM), welches Dioden auch unterstützt.

## Material
- Digitales Multimeter (DMM)

## Refs
- https://de.wikihow.com/LED-Leuchten-testen

## Links
