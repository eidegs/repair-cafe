#klinkenstecker #kopfhörer #audio

Ein 3,5mm Klinkenstecker ist in der Buchse abgebrochen und ein Teilstück steckt dort nun fest.

## Maßnahmen
-  iFixit GripStick

## Material
- iFixit GripStick

## Refs
- https://store.ifixit.de/products/gripstick-headphone-plug-extraction-tool

## Links
