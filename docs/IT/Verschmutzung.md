#IT #reinigen #schmutz #hitze

Durch Verschmutzung können verschiedene Probleme auftreten:
- Abstürze
- Überhitzung
- Laufgeräusche (Lüfter)
- Dysfunktionale Steckverbindungen (z.B. Smartphone Ladebuchse)
- Kurzschlüsse

## Maßnahmen
- Reinigung der betreffenden Stellen/Kontakte

## Material
- Kleiner Staubsauger mit verschiedenen Aufsätzen
- Isopropanol 99.9
- Antistatische Bürsten in versch. Größen
- Mikrobürsten
- Wattestäbchen
- Nadel
- Pinzette
- Druckluftspray oder Staubgebläse

## Refs
- https://de.wikihow.com/Elektrische-Kontakte-reinigen

## Links
