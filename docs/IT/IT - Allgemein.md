#IT 

## Material
- Steckerleiste mit FI/LS
- Antistatik-Matte
- Monitor mit VGA, DVI und HDMI-Anschluss (oder entspr. Adapter/Kabel)
- Tastatur (USB)
- Maus (USB)
- USB auf PS/2 Adapter
- Bitsatz mit verschiedenen Formen und Größen
- PC-Schrauben in versch. Größen
- Datenkabel in versch. Ausführungen (IDE, SATA, USB, ...)
- Kaltgerätekabel in versch. Ausführungen
- Netzteile in versch. Ausführungen (USB-A, USB-C)
- Strommesssgerät (Zwischenstecker)
- Büroklammer
- SIM-Slot Öffner

## Refs
- https://en.wikipedia.org/wiki/USB