#IT #apple #mac #macbook

Seltsames Verhalten bei Apple Macs.

## Maßnahmen
- Zurücksetzen von SMC, PRAM und NVRAM

## Material

## Refs
- [Zurücksetzen von SMC, PRAM und NVRAM am Mac Laptop](https://de.ifixit.com/Anleitung/Zur%C3%BCcksetzen+von+SMC+PRAM+und+NVRAM+am+Mac+Laptop/113252)
- [Zurücksetzen von SMC, PRAM und NVRAM am MacBook Pro Touch Bar 2018](https://de.ifixit.com/Anleitung/Zur%C3%BCcksetzen+von+SMC+PRAM+und+NVRAM+am+MacBook+Pro+Touch+Bar+2018/113258#s216490)
## Links
