#IT#cpu #gpu #prozessor #gtafikkarte #hitze #schmutz #overclocking

- Die CPU taktet aufgrund hoher Temperaturen herunter oder das System stürzt ab.
- Die GPU erzeugt Grafikfehler in 3D-Anwedungen.

## Maßnahmen
- Ort der Hitzeentwicklung bestimmen (DMM, Infrarot Thermometer, Wärmebildkamera)
- CPU-/GPU-Kühlkörper reinigen
- Funktion der Lüfters sicherstellen
- Wäremleitpaste/-pad der CPU tauschen
- Lüfterlager schmieren
- Einstellungen der CPU/GPU auf Standard zurücksetzen (Overclocking)

## Material
- s. [[Verschmutzung]]
- Digitales Multimeter (DMM) mit Temperatursonde
- Infrarot Thermometer
- Wärmebildkamera
- Wärmeleitpaste/-pad
- Feinmechaniköl
- Scalpel oder vgl.

## Refs

## Links
- [[Verschmutzung]]
