#IT #sim #handy #smartphone

Die SIM-Karte in einem Handy/Smartphone funktioniert nicht.

## Maßnahmen
- Einsetzen der SIM in ein anderes Gerät
- Reinigen der Kontakte mit Isopropyl

## Material
- Althandy für Standard- oder Mini-SIM, Micro-SIM und Nano-SIM 
- Isopropanol 99.9

## Refs
- https://www.telekom.de/unterwegs/sim-karten-formate
- https://de.wikipedia.org/wiki/SIM-Karte

## Links
