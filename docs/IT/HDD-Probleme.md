#IT #HDD #festplatte

Es gibt Probleme im Zusammenhang mit der Festplatte.

## Maßnahmen
- SMART-Daten auslesen bzw. Self-test starten
```
apt-get install smartmontools
smartctl -a /dev/sda
smartctl -t short /dev/sda
```

## Material
- Linux Boot-Stick/-CD

## Refs
- https://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl

## Links
