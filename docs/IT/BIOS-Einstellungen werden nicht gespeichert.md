#IT #bios

- Es erscheint ein Warnmeldung bei jedem Hochfahren in Bezug auf das BIOS
- Die BIOS-Einstellungen gehen nach einem Neustart verloren.
- Die Systemzeit wir ständig zurückgesetzt.

## Maßnahmen
- Prüfen der Mainboard-Batterie (Batterietester / Digitales Multimeter)
- Austausch der Batterie gegen eine neue

## Material
- Digitales Multimeter (DMM)

## Refs
- https://de.wikihow.com/Batterien-testen
- https://praxistipps.chip.de/bios-batterie-im-pc-wechseln-so-funktionierts_28122

## Links
