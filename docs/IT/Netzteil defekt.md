#IT #netzteil #power

Der PC läuft instabil oder hat Probleme beim (Kalt)Starten.

ACHTUNG: Das Netzteil nicht öffnen! Die Kondensatoren enthalten Restladungen!

## Maßnahmen
- Durchmessen des Netzteils mit Messgerät
- Tausch des Netzteiles

## Material
- LEAGY Computer PC Power Supply Tester

## Refs
- https://www.amazon.de/dp/B07B52X5R2/

## Links
