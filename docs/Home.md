Willkommen in der Knowledge Base des Repair Café Eidelstedt.

Dieses Projekt basiert auf [Obsidian](https://obsidian.md/).

## Refs
- https://de.ifixit.com/Anleitung | https://www.ifixit.com/Guide
- https://de.wikihow.com/Kategorie:Technik-%26-Elektronik
- https://www.wikihow.com/Category:Computers-and-Electronics